# Combined String Calculator 

*Overview*

This program consumes a string of numbers that is delimited with various characters and does the following computations: 

*For a subtraction operation:*
- Finds the delimiters
- Splits the string 
- Filters numbers over 1000 (using an exception handler)
- Converts the separated strings to integers if there are letters, they are converted to integers
- Performs an arithmatic operation that returns a negative integer

*For an addition operation:*
- Finds the delimiters
- Splits the string 
- Ignores numbers over 1000
- Filters negative numbers (using an exception handler)
- Converts the separated strings to integers
- Performs an arithmatic operation that returns a positive integer

In the test project folder *StringCalculatorTests* see the tests covered for the above computations in the files *AdditionOperationUnitTests.cs* and *SubtractionOperationUnitTests.cs*

*Objective*

The aim with this program was to learn and implement:

- SOILID principles, specifically Single Responsibility, Interfaces Segregation and Dependency  
- Exception Handling
- Test Driven Development(TDD)

*Tech Stack*
- .Net 6 Framework
- Microsoft Test Project with NUnit Framework
- C# 

*Setup*

- Install .net 6.0 framework
- Use nuget package manager to install microsoft tests project, NUnit framework and NSubstitute

 For more detailed info goto https://dotnet.microsoft.com/en-us/download/dotnet-framework