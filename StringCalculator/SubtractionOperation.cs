﻿using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class SubtractionOperation : ICalculator
    {

        private IDelimitersFinder delimitersFinder;
        private IStringSplitter stringSplitter;
        private IConverter converter;
        private IExceptionsHandler exceptionsHandler;

        public SubtractionOperation(IDelimitersFinder _delimitersFinder, IStringSplitter _stringSplitter, IConverter _converter, IExceptionsHandler _exceptionsHandler)
        {
            delimitersFinder = _delimitersFinder;
            stringSplitter = _stringSplitter;
            converter = _converter;
            exceptionsHandler = _exceptionsHandler;
        }
        public int Calculate(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            string[] delimiters = delimitersFinder.getDelimiters(numbers);
            string[] splittedNumbers = stringSplitter.SplitDelimitedString(numbers, delimiters);
            int[] convertedNumbers = converter.ConvertStringNumbersToIntegers(splittedNumbers);

            exceptionsHandler.ThrowExceptionsForNumbersOverThousand(convertedNumbers);

            return Subtract(convertedNumbers);
        }
        public int Subtract(int[] numbers)
        {
            int difference = 0;

            foreach (int number in numbers)
            {
                if (number < 0)
                {
                    return difference -= number * -1;
                }

                difference -= number;
            }

            return difference;
        }
    }
}
