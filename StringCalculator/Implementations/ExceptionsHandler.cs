﻿

using StringCalculator.Interfaces;

namespace StringCalculator.Implementations
{
    public class ExceptionsHandler : IExceptionsHandler
    {
        public void ThrowExceptionsForNegativesNumbers(int[] numbers)
        {
            List<int> negativeNumbers = new List<int>();
            string numberFound;

            foreach (int number in numbers)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number);
                }
            }

            if (negativeNumbers.Count > 0)
            {
                numberFound = String.Join(", ", negativeNumbers.ToArray());

                throw new Exception($"Negative numbers are not allowed, {numberFound}.");
            }
        }

        public void ThrowExceptionsForNumbersOverThousand(int[] numbers)
        {
            List<int> numbersGreaterThan1000 = new List<int>();
            string numberFound;

            foreach (int number in numbers)
            {
                if (number > 1000)
                {
                    numbersGreaterThan1000.Add(number);
                }
            }

            if (numbersGreaterThan1000.Count > 0)
            {
                numberFound = String.Join(", ", numbersGreaterThan1000.ToArray());

                throw new Exception($"Numbers greater than 1000 are not allowed, {numberFound}.");
            }
        }
    }
}
