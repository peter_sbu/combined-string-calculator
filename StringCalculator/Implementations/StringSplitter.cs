﻿

using StringCalculator.Constants;
using StringCalculator.Interfaces;

namespace StringCalculator.Implementations
{
    public class StringSplitter : IStringSplitter
    {
        public string[] SplitDelimitedString(string numbers, string[] delimiters)
        {
            string[] splittedString = SplitString(numbers);
            string[] numbersWithoutDelimiters = numbers.Split(delimiters, StringSplitOptions.None);

           if(numbers.StartsWith(CalculatorConstants.DoubleHash) || numbers.StartsWith(CalculatorConstants.DoubleSlash))
            {
                numbersWithoutDelimiters = splittedString[1].Split(delimiters, StringSplitOptions.None);

                return numbersWithoutDelimiters;
            }

            return numbersWithoutDelimiters;
        }

        public string[] SplitString(string numbers)
        {
            string[] splittedString = numbers.Split(CalculatorConstants.NewLine, StringSplitOptions.None);

            return splittedString;
        }
    }
}
