﻿
using StringCalculator.Constants;
using StringCalculator.Interfaces;

namespace StringCalculator.Implementations
{
    public class Converter : IConverter
    {
        public string ConvertLettersToNumbers(string letterCharacter)
        {
           if(letterCharacter.Length > 1 || !(char.Parse(letterCharacter) >= CalculatorConstants.MinimumASCIICode && char.Parse(letterCharacter) <= CalculatorConstants.MaximumASCIICode))
            {
                return letterCharacter;
            }

           int convertedNumber = char.Parse(letterCharacter) - CalculatorConstants.MinimumASCIICode;

           return convertedNumber.ToString();
        }

        public int[] ConvertStringNumbersToIntegers(string[] numbers)
        {
            int[] convertedNumbers = {};

            foreach (string number in numbers)
            {
                string convertedNumber = ConvertLettersToNumbers(number);

                convertedNumbers = convertedNumbers.Append(int.Parse(convertedNumber)).ToArray();
            }

            return convertedNumbers;
        }
    }
}
