﻿
using StringCalculator.Interfaces;
using StringCalculator.Constants;

namespace StringCalculator.Implementations
{
    public class DelimitersFinder : IDelimitersFinder
    {
        IStringSplitter stringSplitter = new StringSplitter();

        public string[] getDelimiters(string numbers)
        {
            List<string> delimiters = new List<string> {CalculatorConstants.Comma, CalculatorConstants.NewLine};

            if(numbers.StartsWith(CalculatorConstants.DoubleHash) || numbers.StartsWith(CalculatorConstants.DoubleSlash))
            {
                numbers = numbers.Remove(CalculatorConstants.StartPosition, CalculatorConstants.CharactersToRemove);
                string[] splittedString = stringSplitter.SplitString(numbers);

                delimiters.Add(GetCustomDelimiters(splittedString));

                if(numbers.Contains(CalculatorConstants.BackwardsBrackets) || numbers.Contains(CalculatorConstants.OpenBracket))
                {
                    string[] multipleDelimiters = GetMultipleCustomDelimiters(splittedString);

                    return multipleDelimiters;
                }
            }

            if (numbers.Contains(CalculatorConstants.OpenAngelBracket))
            {
                string[] splittedString = stringSplitter.SplitString(numbers);
                string[] variousCustomDelimiters = GetDifferentCustomDelimiters(splittedString);

                return variousCustomDelimiters;
            }

            return delimiters.ToArray();
        }

        private string GetCustomDelimiters(string[] splittedString)
        {
            string customDelimiter = splittedString[0];

            return customDelimiter;
        }

        private string[] GetMultipleCustomDelimiters(string[] splittedString)
        {
            string sectionWithDelimiters = splittedString[0].Trim(CalculatorConstants.OpenBracket, CalculatorConstants.CloseBracket);

            string[] delimitersFound = sectionWithDelimiters.Split(CalculatorConstants.BackwardsBrackets, StringSplitOptions.None);

            return delimitersFound;
        }

        private string[] GetDifferentCustomDelimiters(string[] splittedString)
        {
            string sectionWithDelimiters = splittedString[0].Remove(CalculatorConstants.StartPosition, CalculatorConstants.TotalCharactersToRemove);

            string[] variousDelimiters = {};
            sectionWithDelimiters = sectionWithDelimiters.Trim(sectionWithDelimiters.Last());

            if (sectionWithDelimiters.Contains(CalculatorConstants.OpenAngelBracket))
            {
                string delimiters = sectionWithDelimiters.Trim(CalculatorConstants.OpenAngelBracket, CalculatorConstants.CloseAngelBracket);
                string[] splitterCharacters = {CalculatorConstants.BackwardsAngelBrackets};

                variousDelimiters = stringSplitter.SplitDelimitedString(delimiters, splitterCharacters);

                return variousDelimiters;
            }

            variousDelimiters = variousDelimiters.Append(sectionWithDelimiters).ToArray();

            return variousDelimiters;
        }
    }
}
