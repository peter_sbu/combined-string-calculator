﻿using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class AdditionOperation : ICalculator
    {
        private IDelimitersFinder delimitersFinder;
        private IStringSplitter stringSplitter;
        private IConverter converter;
        private IExceptionsHandler exceptionsHandler;

        public AdditionOperation(IDelimitersFinder _delimitersFinder, IStringSplitter _stringSplitter, IConverter _converter, IExceptionsHandler _exceptionsHandler)
        {
            delimitersFinder = _delimitersFinder;
            stringSplitter = _stringSplitter;
            converter = _converter;
            exceptionsHandler = _exceptionsHandler;
        }
        public int Calculate(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            string[] delimiters = delimitersFinder.getDelimiters(numbers);
            string[] splittedNumbers = stringSplitter.SplitDelimitedString(numbers, delimiters);
            int[] convertedNumbers = converter.ConvertStringNumbersToIntegers(splittedNumbers);

            exceptionsHandler.ThrowExceptionsForNegativesNumbers(convertedNumbers);
            
            return Add(convertedNumbers);
        }

        public int Add(int[] convertedNumbers)
        {
            int sum = 0;
            List<int> numbersOver1000 = new List<int>();

            foreach (int number in convertedNumbers)
            {
                numbersOver1000.Add(number);
                numbersOver1000.RemoveAll(numberToRemove => number > 1000);

                sum = numbersOver1000.Sum();
            }

            return sum;
        }
    }
}
