﻿

namespace StringCalculator.Constants
{
    public class CalculatorConstants
    {
        public const int StartPosition = 0;
        public const int CharactersToRemove = 2;
        public const string NewLine = "\n";
        public const string Comma = ",";
        public const string DoubleHash = "##";
        public const string DoubleSlash = "//";
        public const string BackwardsBrackets = "][";
        public const char OpenBracket = '[';
        public const char CloseBracket = ']';
        public const char OpenAngelBracket = '<';
        public const char CloseAngelBracket = '>';
        public const string BackwardsAngelBrackets = "><";
        public const int TotalCharactersToRemove = 6;
        public const int MaximumASCIICode = 106;
        public const int MinimumASCIICode = 97;
    }
}
