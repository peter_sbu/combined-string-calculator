﻿

namespace StringCalculator.Interfaces
{
    public interface IDelimitersFinder
    {
        string[] getDelimiters(string numbers);
    }
}
