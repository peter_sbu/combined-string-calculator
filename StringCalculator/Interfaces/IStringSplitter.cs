﻿
namespace StringCalculator.Interfaces
{
    public interface IStringSplitter
    {
        string[] SplitDelimitedString(string numbers, string[] delimiters);

        string[] SplitString(string numbers);
    }
}
