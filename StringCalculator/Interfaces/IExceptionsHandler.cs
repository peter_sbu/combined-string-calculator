﻿

namespace StringCalculator.Interfaces
{
    public interface IExceptionsHandler
    {
        void ThrowExceptionsForNumbersOverThousand(int[] numbers);
        void ThrowExceptionsForNegativesNumbers(int[] numbers);
    }
}
