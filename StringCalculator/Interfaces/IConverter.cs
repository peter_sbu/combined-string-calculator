﻿
namespace StringCalculator.Interfaces
{
    public interface IConverter
    {
        int[] ConvertStringNumbersToIntegers(string[] numbers);

        string ConvertLettersToNumbers(string letterCharacter);
    }
}
