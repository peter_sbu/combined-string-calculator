﻿
namespace StringCalculator.Interfaces
{
    public interface ICalculator
    {
        int Calculate(string numbers);
    }
}
