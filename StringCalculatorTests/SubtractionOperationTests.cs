﻿
using NSubstitute;
using NUnit.Framework;
using StringCalculator;
using StringCalculator.Implementations;
using StringCalculator.Interfaces;
using Assesrt = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class SubtractionOperationTests
    {
        IDelimitersFinder delimitersFinder;
        IStringSplitter stringSplitter;
        IConverter converter;
        IExceptionsHandler exceptionsHandler = new ExceptionsHandler();
        SubtractionOperation stringsCalculator;

        [SetUp]
        public void SetUp()
        {
            delimitersFinder = Substitute.For<IDelimitersFinder>();
            stringSplitter = Substitute.For<IStringSplitter>();
            converter = Substitute.For<IConverter>();

            stringsCalculator = new SubtractionOperation(delimitersFinder, stringSplitter, converter, exceptionsHandler);
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Subtracting_Return_Zero()
        {
            //Arrange
            var emptyString = "";
            var expectedResult = 0;

            //Action
            var actualResult = stringsCalculator.Calculate(emptyString);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_DelimitedStringOfNumbers_WHEN_Subtracting_THEN_RETURN_NegativeSum()
        {
            //Arrange
            var numbers = "1\n2,3";
            var delimiters = new[] {",","\n"};
            var splittedString = new[] {"1","2","3"};
            var convertedNumbers = new[] {1,2,3};
            var expectedResult = -6;

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [Test]
        public void GIVEN_CustomDelimitedStringOfNumbers_WHEN_Subtracting_Return_NegativeSum()
        {
            //Arrange
            var numbers = "##;\n1;2";
            var expectedResult = -3;
            var delimiters = new[] {",","\n",";"};
            var splittedString = new[] {"1","2"};
            var convertedNumbers = new[] {1,2};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringWithNumbersGreaterThan1000_WHEN_Subtracting_Return_ThrowExceptionAndListTheNumbersOver1000()
        {
            //Arrange
            var numbers = "1,1001,2004";
            var expectedResult = "Numbers greater than 1000 are not allowed, 1001, 2004.";
            var delimiters = new[] {",","\n"};
            var splittedString = new[] {"1","1001","2004"};
            var convertedNumbers = new[] {1,1001,2004};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);


            //Action
            var actualResult = Assert.Throws<Exception>(() => stringsCalculator.Calculate(numbers));

            //Assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }

        [Test]
        public void GIVEN_StringWithNegativeNumbers_WHEN_Subtracting_Return_SumAndIgnoreNegatives()
        {
            //Arrange
            var numbers = "1,-2,4,-4";
            var expectedResult = -11;
            var delimiters = new[] { ",", "\n" };
            var splittedString = new[] {"1","-2","4","-4"};
            var convertedNumbers = new[] {1,-2,4,-4};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringOfNumbersWithMoreDelimiters_WHEN_Subtracting_Return_NegativeSum()
        {
            //Arrange
            var numbers = "##[***]\n1***2***3";
            var expectedResult = -6;
            var delimiters = new[] {",","\n","***"};
            var splittedString = new[] {"1","2","3"};
            var convertedNumbers = new[] {1,2,3};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringOfNumbersWithMultipleDelimiters_WHEN_Subtracting_Return_NegativeSum()
        {
            //Arrange
            var numbers = "##[*][%]\n1*2%3";
            var expectedResult = -6;
            var delimiters = new[] {"*","%"};
            var splittedString = new[] {"1","2","3"};
            var convertedNumbers = new[] {1,2,3};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithOtherCustomDelimitersAndLetters_WHEN_Subtracting_RETURN_NegativeSum()
        {
            //Arrange
            var numbers = "<(>##(*)\n1*2*3";
            var expectedResult = -6;
            var delimiters = new[] {"*"};
            var splittedString = new[] {"1","2","3"};
            var convertedNumbers = new[] {1,2,3};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Act
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithMoreOtherCustomDelimitersAndLetters_WHEN_Subtracting_RETURN_NegativeSum()
        {
            //Arrange
            string numbers = "<[>##[::)\n3::4::5";
            int expectedResult = -12;
            var delimiters = new[] {"::"};
            var splittedString = new[] {"3","4","5"};
            var convertedNumbers = new[] {3,4,5};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Act
            int actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
