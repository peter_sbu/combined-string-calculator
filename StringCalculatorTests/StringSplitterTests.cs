﻿
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using StringCalculator.Implementations;
using StringCalculator.Interfaces;

namespace StringCalculatorTests
{
    public class StringSplitterTests
    {
        private IStringSplitter stringSplitter;

        [SetUp]
        public void SetUp()
        {
            stringSplitter = new StringSplitter();
        }

        [Test]
        public void GIVEN_StringNumbers_WHEN_Splitting_THEN_RETURN_SplittedString()
        {
            //Arrange
            string numbers = "##;\n1;2";
            string[] expectedResult = {"##;","1;2"};
            //act
            string[] actualResult = stringSplitter.SplitString(numbers);

            //Assert
           Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbers_WHEN_Splitting_THEN_RETURN_SplittedStringOfNumbers()
        {
            //Arrange
            string numbers = "1,2";
            string[] delimiters = {",","\n"};
            string[] expectedResult = {"1","2"};
            //act
            string[] actualResult = stringSplitter.SplitDelimitedString(numbers, delimiters);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
