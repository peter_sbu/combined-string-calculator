﻿
using System;
using NSubstitute;
using NUnit.Framework;
using StringCalculator;
using StringCalculator.Implementations;
using StringCalculator.Interfaces;
using Assesrt = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class AdditionOperationTests
    {
        IDelimitersFinder delimitersFinder;
        IStringSplitter stringSplitter;
        IConverter converter;
        IExceptionsHandler exceptionsHandler = new ExceptionsHandler();
        AdditionOperation stringsCalculator;

        [SetUp]
        public void SetUp()
        {
            delimitersFinder = Substitute.For<IDelimitersFinder>();
            stringSplitter = Substitute.For<IStringSplitter>();
            converter = Substitute.For<IConverter>();

            stringsCalculator = new AdditionOperation(delimitersFinder, stringSplitter, converter, exceptionsHandler);
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_Return_Zero()
        {
            //Arrange
            var emptyString = "";
            var expectedResult = 0;

            //Action
            var actualResult = stringsCalculator.Calculate(emptyString);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_DelimitedStringOfNumbers_WHEN_Adding_THEN_RETURN_Sum()
        {
            //Arrange
            var numbers = "1\n2,3";
            var delimiters = new[] { ",", "\n" };
            var splittedString = new[] { "1", "2", "3" };
            var convertedNumbers = new[] { 1, 2, 3 };
            var expectedResult = 6;

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [Test]
        public void GIVEN_CustomDelimitedStringOfNumbers_WHEN_Adding_Return_Sum()
        {
            //Arrange
            var numbers = "//;\n1;2";
            var expectedResult = 3;
            var delimiters = new[] {",","\n",";"};
            var splittedString = new[] {"1","2"};
            var convertedNumbers = new[] {1,2};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringOfNumbersWithNegatives_WHEN_Adding_Return_ThrowExceptionAndListTheNegativeNumbers()
        {
            //Arrange
            var numbers = "1,-2,4,-4";
            var expectedResult = "Negative numbers are not allowed, -2, -4.";
            var delimiters = new[] {",","\n"};
            var splittedString = new[] {"1","-2","4","-4"};
            var convertedNumbers = new[] {1,-2,4,-4};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);
           

            //Action
            var actualResult = Assert.Throws<Exception>(() => stringsCalculator.Calculate(numbers));

            //Assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }

        [Test]
        public void GIVEN_StringWithNumbersGreaterThan1000_WHEN_Adding_Return_SumAndIgnoreNumbersOver1000()
        {
            //Arrange
            var numbers = "1,1001,4";
            var expectedResult = 5;
            var delimiters = new[] { ",", "\n" };
            var splittedString = new[] {"1","1001","4"};
            var convertedNumbers = new[] {1,1001,4};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringOfNumbersWithMoreDelimiters_WHEN_Adding_Return_Sum()
        {
            //Arrange
            var numbers = "//***\n1***2***3";
            var expectedResult = 6;
            var delimiters = new[] {",","\n","***"};
            var splittedString = new[] {"1","2","3"};
            var convertedNumbers = new[] {1,2,3};

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringOfNumbersWithMultipleDelimiters_WHEN_Adding_Return_Sum()
        {
            //Arrange
            var numbers = "//[*][%]\n1*2%3";
            var expectedResult = 6;
            var delimiters = new[] {"*","%"};
            var splittedString = new[] { "1", "2", "3" };
            var convertedNumbers = new[] { 1, 2, 3 };

            //Mocks
            delimitersFinder.getDelimiters(numbers).Returns(delimiters);
            stringSplitter.SplitDelimitedString(numbers, delimiters).Returns(splittedString);
            converter.ConvertStringNumbersToIntegers(splittedString).Returns(convertedNumbers);

            //Action
            var actualResult = stringsCalculator.Calculate(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
