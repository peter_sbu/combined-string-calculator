﻿
using StringCalculator.Interfaces;
using StringCalculator.Implementations;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class ExceptionsHandlerTest
    {
        IExceptionsHandler exceptionsHandler;

        [SetUp] 
        public void SetUp() 
        {
            exceptionsHandler= new ExceptionsHandler();
        }

        [Test]
        public void GIVEN_ArrayOfNumbersWithThoseGreaterThan1000_WHEN_HandlingExceptions_THEN_ThrowExceptionAndListNumbersGreaterThan1000()
        {
            //Arrange
            int[] numbers = {1,2000,1001};
            var expectedResult = "Numbers greater than 1000 are not allowed, 2000, 1001.";

            //Act
            var actualResult = Assert.Throws<Exception>(() => exceptionsHandler.ThrowExceptionsForNumbersOverThousand(numbers));

            //Assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }

        [Test]
        public void GIVEN_ArrayOfNumbersWithNegativeIntegers_WHEN_HandlingExceptions_THEN_ThrowExceptionAndListNegativeIntegers()
        {
            //Arrange
            int[] numbers = {1,-2,4,-6};
            var expectedResult = "Negative numbers are not allowed, -2, -6.";

            //Act
            var actualResult = Assert.Throws<Exception>(() => exceptionsHandler.ThrowExceptionsForNegativesNumbers(numbers));

            //Assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }
    }
}
