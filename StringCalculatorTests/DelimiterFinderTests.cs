﻿
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using StringCalculator.Interfaces;
using StringCalculator.Implementations;
using NSubstitute;


namespace StringCalculatorTests
{
    [TestFixture]
    public class DelimiterFinderTests
    {
        private IDelimitersFinder delimitersFinder = new DelimitersFinder();

        [Test]
        public void GIVEN_StringNumbersWithDelimiters_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "1,2";
            string[] expectedResult = {",","\n"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithDelimitersAndNewline_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "1\n2,3";
            string[] expectedResult = {",","\n"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithCustomDelimitersAndNewline_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "//;\n1;2";
            string[] expectedResult = {",","\n",";"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithCustomDelimitersOFAnyLength_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "//***\n1***2***3";
            string[] expectedResult = {",","\n","***"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithDifferentCustomDelimiters_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "##[***]\n1***2***3";
            string[] expectedResult = {"***"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithMultipleCustomDelimiters_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "//[*][%]\n1*2%3";
            string[] expectedResult = {"*","%"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithVariousCustomDelimiters_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "<(>##(*)\n1*2*3";
            string[] expectedResult = {"*"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithVariousCustomDelimitersOverOne_WHEN_FindingDelimister_THEN_RETURN_Delimiters()
        {
            //Arrange
            string numbers = "<<>>##<$$$><###>\n5$$$6###7";
            string[] expectedResult = {"$$$","###"};

            //Act
            string[] actualResult = delimitersFinder.getDelimiters(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
