﻿
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using StringCalculator.Interfaces;
using StringCalculator.Implementations;

namespace StringCalculatorTests
{
    public class ConverterTests
    {
        private IConverter converter;

        [SetUp]
        public void SetUp()
        {
            converter = new Converter();
        }

        [Test]
        public void GIVEN_StringLetter_WHEN_Converting_THEN_RETURN_CorrespondingNumberToTheLetter()
        {
            //Arrange
            string letterCharacter = "f";
            string expectedResult = "5";

            //Act
            string actualResult = converter.ConvertLettersToNumbers(letterCharacter);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringArrayOfNumbersAndLetters_WHEN_Converting_THEN_RETURN_ArrayOfIntegersOnly()
        {
            //Arrange
            string[] numbers = {"1","d","6","j"};
            int[] expectedResult = {1,3,6,9};

            //Act
            int[] actualResult = converter.ConvertStringNumbersToIntegers(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
